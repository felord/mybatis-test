package cn.felord.mybatis;

import cn.felord.mybatis.entity.Stu;
import cn.felord.mybatis.entity.Student;
import cn.felord.mybatis.enums.GenderTypeEnum;
import cn.felord.mybatis.mapper.StudentMapper;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MybatisTestApplicationTests {
    @Resource
    private StudentMapper studentMapper;

    @Test
    public void typeHandlerTest() {
        Stu stu = new Stu();
        stu.setStudentName("enum");
        stu.setGenderType(GenderTypeEnum.FEMALE);
        stu.setAge(22);

        Assertions.assertThat(studentMapper.saveStu(stu)).isEqualTo(1);

        Stu st = new Stu();
        st.setStudentName("Tim");
        st.setGenderType(GenderTypeEnum.MALE);
        st.setAge(44);

        Assertions.assertThat(studentMapper.saveAutomaticStu(st)).isEqualTo(1);


        List<Student> all = studentMapper.findAll();
        Assertions.assertThat(all.size()).isEqualTo(5);

        all.forEach(System.out::println);

        List<Stu> allStu = studentMapper.findAllStu();

        Assertions.assertThat(allStu.size()).isEqualTo(5);
        allStu.forEach(System.out::println);

    }

}
