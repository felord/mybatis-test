package cn.felord.mybatis.entity;

import cn.felord.mybatis.enums.GenderType;
import lombok.Data;

/**
 * @author Dax
 * @since 13:24  2019-07-30
 */
@Data
public class Student {
    private Integer studentId;
    private String studentName;
    private GenderType genderType;
    private Integer age;

}
