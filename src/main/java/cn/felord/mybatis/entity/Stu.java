package cn.felord.mybatis.entity;

import cn.felord.mybatis.enums.GenderTypeEnum;
import lombok.Data;

/**
 * @author Dax
 * @since 16:40  2019-07-30
 */
@Data
public class Stu {
    private Integer studentId;
    private String studentName;
    private GenderTypeEnum genderType;
    private Integer age;


}
