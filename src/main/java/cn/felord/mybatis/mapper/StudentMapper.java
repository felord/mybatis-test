package cn.felord.mybatis.mapper;

import cn.felord.mybatis.entity.Stu;
import cn.felord.mybatis.entity.Student;
import cn.felord.mybatis.type.GenderTypeEnumTypeHandler;

import java.util.List;

/**
 * The interface Student mapper.
 *
 * @author Dax
 * @since 13 :21  2019-07-30
 */
public interface StudentMapper {


    /**
     * 测试 {@link org.apache.ibatis.type.EnumOrdinalTypeHandler}.
     *
     * @return the list
     */
    List<Student> findAll();

    /**
     * 测试mapper文件中使用 {@link GenderTypeEnumTypeHandler}.
     *
     * @return the list
     */
    List<Stu> findAllStu();


    /**
     * 测试 在插入sql占位符使用  {@link GenderTypeEnumTypeHandler}.
     *
     * @param stu the stu
     * @return the int
     */
    int saveStu(Stu stu);


    /**
     * 测试{@link GenderTypeEnumTypeHandler} 自动注册 后根据Jdbc 和Java 类型 自动映射.
     *
     * @param stu the stu
     * @return the int
     */
    int saveAutomaticStu(Stu stu);

}
