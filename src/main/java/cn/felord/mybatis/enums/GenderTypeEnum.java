package cn.felord.mybatis.enums;

/**
 * @author Dax
 * @since 16:40  2019-07-30
 */
public enum GenderTypeEnum {
    /**
     * female.
     */
    FEMALE(0, "女"),
    /**
     * male.
     */
    MALE(1,"男"),
    /**
     * unknown.
     */
    UNKNOWN(2, "未知");

    private int value;
    private String description;

    GenderTypeEnum(int value, String description) {
        this.value = value;
        this.description = description;
    }


    public int value() {
        return this.value;
    }


    public String description() {
        return this.description;
    }
}
