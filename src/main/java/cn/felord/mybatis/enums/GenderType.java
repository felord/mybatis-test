package cn.felord.mybatis.enums;

/**
 * The enum Gender type.
 *
 * @author Dax
 * @since 13 :44  2019-07-30
 */
public enum GenderType {
    /**
     * Female gender type.
     */
    FEMALE,
    /**
     * Male gender type.
     */
    MALE,
    /**
     * Unknown gender type.
     */
    UNKNOWN

}
